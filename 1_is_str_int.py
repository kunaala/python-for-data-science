####COMPLETE##########
def is_string_integer(string1):
    '''
    used to check if the inputted string is a valid integer in decimal format
    e.g "1", "21", 10999" return True
        "a", "b", ""my_name" return false
    '''
    #try:
    assert  (isinstance(string1, str) and (len(string1) == 1))  
    # except AssertionError:
        # print("please input a valid string")
        # '''
        # returns -1 indicating unexpected return of the function
        # '''
        # return -1
   
    #print(string1)
    try:
        v = int(string1)
        #print(v)
        return isinstance(v, int)
    except ValueError:
        return False



#print(is_string_integer("2"))