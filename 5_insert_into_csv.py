####COMPLETE##########
def write_columns(data,fname):
    '''
    To enter the following formula to three columns to a comma-separated file:

            data_value, data_value**2, (data_value+data_value**2)/3. 
    '''
    assert isinstance(data, list)
    for i in data: assert (isinstance(i,int) or isinstance(i,float))
    assert isinstance(fname, str)
    c1 = list(map(lambda x: format(x,".2f"), data))
    c2 = list(map(lambda x: format((x **2), ".2f"), data))
    c3 = list(map(lambda x: format((x + x**2)/3,".2f"), data))
   
    l = list(zip(c1,c2,c3))
    f = open(fname, "w")
    for i in l:
        f.write("{0}, {1}, {2}\n".format(i[0], i[1], i[2]))
        
    f.close()


#write_columns([5,4,6,1,9,0,3,9,2,7,10,8,4,7,1,2,7.04,0,5,2.0,8,2,0,1,1,1,2,10,6,2],"/home/kunaala/python_for_ds/egcsv.txt" )