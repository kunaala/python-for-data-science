#####COMPLETE##########
def write_chunks_of_five(words,fname):
    """
    Function to create list of chunks of words from a list of 10000 words in a separate list
    Sample follows

    the of and to a
    in for is on that
    by this with i you
    it not or be are
    from at as your all
    have new more an was
    we will home can us
    about if page my has
    search free but our one
    other do no information time
    
    """
    assert ( isinstance(words,list) and isinstance(fname,str))
    for i in words: assert(isinstance(i, str)) 
    
    size = len(words)
    f = open(fname, "w")
    '''
    iterate through the list for multiples of 5 only
    We'll handle the residuals in step 2
    '''
    for i in range(0,(5 *(size//5)),5):   
        f.write("{0} {1} {2} {3} {4}\n".format(words[i], words[i +1], words[i+2], words[i+3], words[i +4]))
    '''
    Step 2
    iterate through the words from multiples of 5 and onwards
    i.e, for 53 words iterate from 50 to 53'''
    if(size%5 !=0):
        for i in range((5 * (size//5)),size):
            # print(words[i])
            f.write(words[i])
            f.write(" ") 
    f.close()



# s = "the people of India and to a in for is on that by this with i you it not or be are from at as your all have new more an was we will home can us about if page my has search free but our one other do no information time"
# l = s.split()
# write_chunks_of_five(l,"eg.txt")