####COMPLETE##########
def compute_average_word_length(instring,unique=False):

    '''
    this function computes the average length of the words in the input string (instring)
    If the unique option is set to True ( False by default ), it excludes duplicated words.
    '''
    # check for instring being of type string
    # try:
    #     assert isinstance(instring,str)
    # except AssertionError:
    #     print("please input a valid string")
    #     '''
    #     returns -1 indicating unexpected return of the function
    #     '''
    #     return -1
    # check for unique being of type boolean
    assert isinstance(instring,str)
    assert isinstance(unique,bool)
    # try:
    #     assert isinstance(unique,bool)
    # except AssertionError:
    #     print("please input a boolean value as 2nd argument")
    #     '''
    #     returns -1 indicating unexpected return of the function
    #     '''
    #     return -1
    '''
    Split the words in the string into elements of a list of strings
    '''
    raw = instring.split()
    l = [] 
    total = 0
    if(unique == False):
        #print(instring.split())
        #print(raw)
        for i in raw:
            total += len(i)
        avg = total/len(raw)
        # print("avg word length is:", avg )
        return avg
    else: 
        [l.append(i.lower()) for i in raw if i.lower() not in l]
        for i in l:
            total += len(i)
        avg = total/len(l)
        # print(l)
        # print("avg word length is:", avg )
        return avg
        



#compute_average_word_length("The", True)
