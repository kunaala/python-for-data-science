####COMPLETE##########
def convert_hex_to_RGB(color_list):
    '''
    converts a list of color hex-codes (e.g., ['#FFAABB']), to a list of RGB-tuples. For example, [(255,170,187)]
    '''
    assert isinstance(color_list,list)
    # try:
    #     assert isinstance(color_list,list)
    # except AssertionError:
    #     print("please input a list of strings")
    #     '''
    #     returns -1 indicating unexpected return of the function
    #     '''
    #     return -1
    rgb_list = []
    for i in color_list:
        #print(color_list[i])
        '''
        Ensure the given color string is a valid hex code suitable for conversion into RGB
        '''
        assert(len(i) == 7)
        assert (i[0] == "#")
        r,g,b = int(i[1:3],16), int(i[3:5],16), int(i[5:7],16)
        #print(r,g,b)
        '''
        append the rgb channel values i.e, RGB tuples to the list
        '''
        rgb_list.append((r,g,b))
        #print(rgb_list)
    # print(rgb_list)
    return rgb_list
    

# convert_hex_to_RGB(["#ffaacd", "#cdaaff", "#aaffcd"])
