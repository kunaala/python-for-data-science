#####COMPLETE###############
import itertools as it
def get_power_of3(a):
    '''
    Given a list of weights
    w = [1,3,9,27]
    Represent the given input (range of (1,40)) in terms of linear multiples of weights 
    *******NOTE**************
    **The elements cannot be reused again**
    '''
    assert isinstance(a,int) and (a >= 1 and a <=40)
    weights = [1,3,9,27]
    combos = [0,0,0,1,1,1,1,-1,-1,-1]
    superset = list(it.permutations(combos,4))
    prod =[]
    for i in superset:
        sum =0
        for j in range(4):
            sum +=i[j] *weights[j] 
        prod.append(sum)
    for i in prod:
        if(a == i):
            return list(superset[prod.index(i)])

print(get_power_of3(2))
